@extends('layouts.app')

@section('title')
{{__('Create User Shift')}}
@endsection

@section('breadcrumb')
<div class="content-header">
    <div class="container-fluid" id="myHeaderLinks">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1 class="m-0 text-dark">
            <i class="nav-icon fas fa-user-injured"></i>
            {{__('User Shift')}}
          </h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
@endsection

@section('content')
<div class="card card-primary" id="page-wrap">
    <div class="card-header">
      <h3 class="card-title">{{__('General')}}</h3>
    </div>
    <form method="POST" action="{{route('admin.usershift.store')}}"  enctype="multipart/form-data"  id="form">
        <!-- /.card-header -->
        <div class="card-body">
            @csrf
            @include('admin.usershift._form')
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
        </div>
    </form>	

</div>
 
<div class="row">
	<div class="card card-primary col-md-6">
		<div class="card-header">
		  <h3 class="card-title">{{__('Superviser Open Shift List')}}</h3>
		</div>
		
		<table id="" class="table table-striped table-hover table-bordered display" width="100%">
            <thead>
            
			<tr>
              <th>Shift No:</th>
              <th>{{__('Lab Name')}}</th>
              <th>{{__('Shift Prefix')}}</th>
              <th>{{__('Shift Date')}}</th>
              <th>{{__('Status')}}</th>
            </tr>
            </thead>
            <tbody>
				@foreach($data['supervisor_shifts'] as $single)
					<tr>
					<td>{{$single->id}}</td>
					<td>{{$single->lab_name_eng}}</td>
					<td>{{$single->shift_prefix}}</td>
					<td>{{$single->shift_date}}</td>
					<td>{{$single->status_label}}</td>
				@endforeach	
			</tr>
            </tbody>
          </table>
	</div>
	<div class="card card-primary col-md-6">
		<div class="card-header">
		  <h3 class="card-title">{{__('User Shift List')}}</h3>
		</div>
		
		<table id="table" class="table table-striped table-hover table-bordered" width="100%">
            <thead>
            <tr>
              <th>Shift No:</th>
              <th>{{__('Lab Name')}}</th>
              <th>{{__('Shift Prefix')}}</th>
              <th>{{__('Added By')}}</th>
              <th>{{__('Status')}}</th>
              <th width="100px">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
                
            </tbody>
          </table>
	</div>
</div>
@endsection
@section('scripts')
  <script src="{{url('public/js/admin/usershift.js')}}"></script>
  <script src="{{url('public/js/admin/usershift2.js')}}"></script>
@endsection